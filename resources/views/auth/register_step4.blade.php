<x-guest-layout>
    <x-auth-card>
        <x-slot name="header">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Register - Conclusion - Step 4') }}
            </h2>
        </x-slot>

        <div>
            {{ __('Congatulations, your registration is now complete!') }}
        </div>

        <div>
            {{ __('Your payment ID is:') }}
            {{ $payment_data_id }}
        </div>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('register.step4.store') }}">
            @csrf

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-button class="ml-4">
                    {{ __('Continue to Dashboard') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>

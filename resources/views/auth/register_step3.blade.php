<x-guest-layout>
    <x-auth-card>
        <x-slot name="header">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Register - Payment Information - Step 3') }}
            </h2>
        </x-slot>

        <div class="font-medium text-red-600">
            {{ session('message') }}
        </div>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('register.step3.store') }}">
            @csrf

            <!-- Account Owner -->
            <div>
                <x-label for="account_owner" :value="__('Account Owner')" />

                <x-input id="account_owner" class="block mt-1 w-full" type="text" name="account_owner" :value="old('account_owner') ?? $account_owner" required autofocus />
            </div>

            <!-- IBAN -->
            <div>
                <x-label for="iban" :value="__('IBAN')" />

                <x-input id="iban" class="block mt-1 w-full" type="text" name="iban" :value="old('iban')" required autofocus />
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-button class="ml-4">
                    {{ __('Next') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>

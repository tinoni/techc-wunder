<x-guest-layout>
    <x-auth-card>
        <x-slot name="header">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Register - Address Information - Step 2') }}
            </h2>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('register.step2.store') }}">
            @csrf

            <!-- Street Address -->
            <div>
                <x-label for="street_address" :value="__('Street Address')" />

                <x-input id="street_address" class="block mt-1 w-full" type="text" name="street_address" :value="old('street_address')" required autofocus />
            </div>

            <!-- House Number -->
            <div>
                <x-label for="house_number" :value="__('House Number')" />

                <x-input id="house_number" class="block mt-1 w-full" type="text" name="house_number" :value="old('house_number')" required autofocus />
            </div>

            <!-- Zip Code -->
            <div>
                <x-label for="zip" :value="__('Zip Code')" />

                <x-input id="zip" class="block mt-1 w-full" type="text" name="zip" :value="old('zip')" required autofocus />
            </div>

            <!-- City -->
            <div>
                <x-label for="city" :value="__('City')" />

                <x-input id="city" class="block mt-1 w-full" type="text" name="city" :value="old('city')" required autofocus />
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-button class="ml-4">
                    {{ __('Next') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>

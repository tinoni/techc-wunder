<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\Auth\EmailVerificationPromptController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\auth\RegisteredUserStep2Controller;
use App\Http\Controllers\auth\RegisteredUserStep3Controller;
use App\Http\Controllers\auth\RegisteredUserStep4Controller;
use App\Http\Controllers\Auth\VerifyEmailController;
use Illuminate\Support\Facades\Route;

Route::get('/register', [RegisteredUserController::class, 'create'])
                ->middleware(['registration.complete', 'guest'])
                ->name('register');

Route::post('/register', [RegisteredUserController::class, 'store'])
                ->middleware('guest');

Route::get('register/step2', [RegisteredUserStep2Controller::class, 'create'])
                ->middleware(['auth', 'registration.complete'])
                ->name('register.step2');

Route::post('register/step2', [RegisteredUserStep2Controller::class, 'store'])
                ->middleware('auth')
                ->name('register.step2.store');

Route::get('register/step3', [RegisteredUserStep3Controller::class, 'create'])
                ->middleware(['auth', 'registration.complete'])
                ->name('register.step3');

Route::post('register/step3', [RegisteredUserStep3Controller::class, 'store'])
                ->middleware('auth')
                ->name('register.step3.store');

Route::get('register/step4', [RegisteredUserStep4Controller::class, 'create'])
                ->middleware(['auth', 'registration.complete'])
                ->name('register.step4');

Route::post('register/step4', [RegisteredUserStep4Controller::class, 'store'])
                ->middleware('auth')
                ->name('register.step4.store');

Route::get('/login', [AuthenticatedSessionController::class, 'create'])
                ->middleware('guest')
                ->name('login');

Route::post('/login', [AuthenticatedSessionController::class, 'store'])
                ->middleware('guest');

Route::get('/forgot-password', [PasswordResetLinkController::class, 'create'])
                ->middleware('guest')
                ->name('password.request');

Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
                ->middleware('guest')
                ->name('password.email');

Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])
                ->middleware('guest')
                ->name('password.reset');

Route::post('/reset-password', [NewPasswordController::class, 'store'])
                ->middleware('guest')
                ->name('password.update');

Route::get('/verify-email', [EmailVerificationPromptController::class, '__invoke'])
                ->middleware('auth')
                ->name('verification.notice');

Route::get('/verify-email/{id}/{hash}', [VerifyEmailController::class, '__invoke'])
                ->middleware(['auth', 'signed', 'throttle:6,1'])
                ->name('verification.verify');

Route::post('/email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
                ->middleware(['auth', 'throttle:6,1'])
                ->name('verification.send');

Route::get('/confirm-password', [ConfirmablePasswordController::class, 'show'])
                ->middleware('auth')
                ->name('password.confirm');

Route::post('/confirm-password', [ConfirmablePasswordController::class, 'store'])
                ->middleware('auth');

Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
                ->middleware('auth')
                ->name('logout');

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegistrationComplete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $route_name = $request->route()->getName();

        if(Auth::user()){
            $step = Auth::user()->registration_step;
            switch ($step) {
                case 0:
                    if ($route_name == 'register.step2') {
                        return $next($request);
                    }
                    return redirect('/register/step2');
                    break;
                case 1:
                    if ($route_name == 'register.step3') {
                        return $next($request);
                    }
                    return redirect('/register/step3');
                    break;
                case 2:
                    if ($route_name == 'register.step4') {
                        return $next($request);
                    }
                    return redirect('/register/step4');
                    break;
            }
        }
        return $next($request);
    }
}

<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegisteredUserStep2Controller extends Controller
{

    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register_step2');
    }

    public function store(Request $request)
    {
        $data = $request->only(['street_address', 'house_number', 'zip', 'city']);
        $data['registration_step'] = 1;
        auth()->user()->update($data);
        return redirect()->route('register.step3');
    }

}

<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;


class RegisteredUserStep3Controller extends Controller
{

    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $default_name = Auth::user()->first_name . ' ' . Auth::user()->last_name;
        return view('auth.register_step3', ['account_owner' => $default_name]);
    }


    /**
     * Save user data from step 3
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $data = $request->only(['account_owner', 'iban']);

        $remote_data = [
            'customerId' => Auth::user()->id,
            'iban' => $request->iban,
            'owner' => $request->account_owner,
        ];
        $response = Http::post(
            'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data',
            $remote_data
        );

        if ($response->failed()) {
            return back()->withInput()->with('message', __('Something went wrong, please retry.')); //on error
        }

        $res_data = $response->json();
        $data['payment_data_id'] = $res_data['paymentDataId'];
        $data['registration_step'] = 2;
        auth()->user()->update($data);

        return redirect()->route('register.step4'); //success
    }

}

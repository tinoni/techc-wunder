<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;


class RegisteredUserStep4Controller extends Controller
{

    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register_step4', ['payment_data_id' => Auth::user()->payment_data_id]);
    }


    /**
     * Save user data from step 4
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        event(new Registered(auth()->user()));
        return redirect()->route('dashboard'); //success
    }

}
